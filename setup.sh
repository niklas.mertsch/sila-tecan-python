#!/bin/sh
set -ex

# Environment: Ubuntu, Python >=3.7, .NET 5.0
# don't forget to initialize the submodules: `git submodule update --init --recursive`

# prepare SiLA server
# follows https://gitlab.com/SiLA2/vendors/sila_tecan/-/blob/master/Examples/SampleServer/Readme.md
# add target framework 'net5.0' where required using sed
dotnet build sila_tecan/Examples/SampleServer/SampleImplementation/SampleImplementation.csproj

sed -i 's/<TargetFrameworks>net472;netcoreapp3\.1<\/TargetFrameworks>/<TargetFrameworks>net472;netcoreapp3.1;net5.0<\/TargetFrameworks>/' sila_tecan/Generator/Generator.csproj
sed -i 's/<TargetFramework>netcoreapp2.0<\/TargetFramework>/<TargetFrameworks>netcoreapp2.0;net5.0<\/TargetFrameworks>/' sila_tecan/Examples/SampleServer/SampleService/SampleService.csproj
dotnet build sila_tecan/Generator/Generator.csproj
dotnet run --project sila_tecan/Generator/Generator.csproj --framework net5.0 -- generate-server --root-namespace SampleService sila_tecan/Examples/SampleServer/SampleImplementation/bin/Debug/netstandard2.0/SampleImplementation.dll  sila_tecan/Examples/SampleServer/SampleService/

dotnet build sila_tecan/Examples/SampleServer/SampleService/SampleService.csproj

# generate proto file
mkdir -p proto
xsltproc sila_base/xslt/fdl2proto.xsl sila_tecan/Examples/SampleServer/SampleService/SampleService.sila.xml > proto/SampleService.proto

# generate protobuf/grpc python code and fix imports using sed
mkdir -p py_generated
python3 -m grpc_tools.protoc --proto_path=./proto --proto_path=./sila_base/protobuf --python_out=./py_generated --grpc_python_out=./py_generated proto/SampleService.proto sila_base/protobuf/SiLAFramework.proto
sed -i -r 's/(^import .* as .*__pb2)/from . \1/' py_generated/*.py
