Environment: Ubuntu 20.04 with Python 3.8 and .NET 5 SDK

Steps:
```
git clone https://gitlab.gwdg.de/niklas.mertsch/sila-tecan-python
cd sila-tecan-python
git submodule update --init --recursive
sudo apt update
sudo apt install -y xsltproc python3 python3-pip
python3 -m pip install grpcio grpcio-tools zeroconf
sh setup.sh  # prepare proto, python-grpc stubs and sila_tecan SampleService
python3 run.py
```
