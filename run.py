#!/usr/bin/env python3
import time
from os import remove
from os.path import isfile
from subprocess import Popen

import grpc
from zeroconf import ServiceBrowser, Zeroconf, ServiceListener

from py_generated.SampleService_pb2 import (
    Get_Version_Parameters,
    ToUppercase_Parameters,
    Subscribe_CurrentTime_Parameters,
    CountTo_Parameters,
)
from py_generated.SampleService_pb2_grpc import SampleServiceStub
from py_generated.SiLAFramework_pb2 import String, Integer

# start server
log_file = "server_out.log"
if isfile(log_file):
    remove(log_file)

server_process = Popen(
    f"sila_tecan/Examples/SampleServer/SampleService/bin/Debug/net5.0/SampleService > {log_file}",
    shell=True,
)
while not isfile(log_file):
    time.sleep(0.1)
while "Announcing as" not in open(log_file).read() and "Address already in use" not in open(log_file).read():
    time.sleep(0.1)


# use zeroconf to find the server
class SilaZeroConfListener(ServiceListener):
    service_addresses = []

    def remove_service(self, zc: 'Zeroconf', type_: str, name: str) -> None:
        pass

    def update_service(self, zc: 'Zeroconf', type_: str, name: str) -> None:
        pass

    def add_service(self, zc, type_, name):
        info = zc.get_service_info(type_, name)
        if info.type == "_sila._tcp.local.":
            self.service_addresses.append(f"{'.'.join(str(i) for i in info.addresses[0])}:{info.port}")


listener = SilaZeroConfListener()
ServiceBrowser(Zeroconf(), '_sila._tcp.local.', SilaZeroConfListener())
while not SilaZeroConfListener.service_addresses:
    time.sleep(0.1)

# connect stub to server
stub = SampleServiceStub(grpc.insecure_channel(SilaZeroConfListener.service_addresses[0]))

# unobservable property
print("Unobservable property 'Version'")
version = stub.Get_Version(Get_Version_Parameters()).Version.value
print(f"  SampleService.Version -> {version!r}")

# unobservable command
print("Unobservable command 'ToUppercase'")
param = "abc"
uppercase = stub.ToUppercase(
    ToUppercase_Parameters(Input=String(value=param))
).Uppercase.value
print(f"  SampleService.ToUppercase({param!r}) -> {uppercase!r}")

# observable property
print("Observable property 'CurrentTime'")
time_stream = map(
    lambda response: response.CurrentTime.value,
    stub.Subscribe_CurrentTime(Subscribe_CurrentTime_Parameters()),
)
for _ in range(3):  # dirty: don't cancel subscription
    current_time = next(time_stream)
    print(f"  SampleService.CurrentTime -> {current_time!r}")

# observable command
print("Observable command 'CountTo'")
target, message = 5, "My message"
exec_uuid_msg = stub.CountTo(
    CountTo_Parameters(Target=Integer(value=target), Message=String(value=message))
).commandExecutionUUID
exec_uuid = exec_uuid_msg.value
print(f"  SampleService.CountTo({target!r}, {message!r}) -> {exec_uuid!r}")

info_stream = stub.CountTo_Info(exec_uuid_msg)
intermediate_response_stream = stub.CountTo_Intermediate(exec_uuid_msg)

print("ExecutionInfo and IntermediateResponses")
for info, intermediate_response in zip(info_stream, intermediate_response_stream):
    status = info.CommandStatus.Name(info.commandStatus)
    progress = info.progressInfo.value
    estimated_remaining_time = (
        info.estimatedRemainingTime.seconds + info.estimatedRemainingTime.nanos / 1e9
    )
    updated_lifetime_of_execution = (
        info.updatedLifetimeOfExecution.seconds
        + info.updatedLifetimeOfExecution.nanos / 1e9
    )
    print(
        f"  SampleService.CountTo_Info({exec_uuid!r}) -> ExecutionInfo(commandStatus={status!r}, "
        f"progressInfo={progress!r}, estimatedRemainingTime={estimated_remaining_time!r}, "
        f"updatedLifetimeOfExecution={updated_lifetime_of_execution!r})"
    )

    struct = intermediate_response.Intermediate.CountToIntermediates
    value = struct.CurrentValue.value
    message = struct.Message.value
    print(f"  SampleService.CountTo_Intermediate({exec_uuid!r}) -> CountTo_IntermediateResponses(CurrentValue={value!r}, Message={message!r})")

print("Responses")
response = stub.CountTo_Result(exec_uuid_msg).CountToResponses.CountToResponses
value = response.FinalValue.value
message = response.FinalMessage.value
print(
    f"  SampleService.CountTo_Result({exec_uuid!r}) -> CountTo_Responses(FinalValue={value!r}, FinalMessage={message!r})"
)
